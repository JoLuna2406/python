#coding utf-8
print("MAQUINA DEL TIEMPO")
any1 = int(input("¿En que año estás?: "))
any2 = int(input("¿A que año vas?: "))
if any1 == any2:
    print("No hay años de diferencia.")
    print("No hace falta que viajes en el tiempo.")
elif any1 < any2:
    print("Faltan", any2-any1 ,"años para el año", any2,".")
    viaje = input("¿Quieres hackear el tiempo? (s/n) : ")
    if viaje == "s":
        print("Pues espera",any2-any1,"años.")
    else:
        print("Viaje cancelado.")
else:
    print("Han pasado", any1-any2 ,"años desde el año", any2,".")
    viaje = input("¿Quieres hackear el tiempo? (s/n) : ")
    if viaje == "s":
        print("Buena suerte, no se puede viajar atras en el tiempo.")
    else:
        print("Viaje cancelado.")