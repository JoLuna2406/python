#coding utf-8
print("NOTAS")
nota = int(input("Introduce una nota de 0 a 10: "))
if nota < 0:
    print("Introduce una nota valida.")
elif nota <= 4:
    print("Insuficiente.")
elif nota <= 5:
    print("Suficiente.")
elif nota <= 6:
    print("Bien.")
elif nota <= 8:
    print("Muy bien.")
elif nota <= 10:
    print("Excelente.")
else:
    print("Introduce una nota valida.")